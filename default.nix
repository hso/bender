let
  pkgs = import <nixpkgs> {};
in
{ stdenv ? pkgs.stdenv }:

pkgs.python3Packages.buildPythonPackage {
  name = "bender";
  version = "0.0.3";
  src = if pkgs.lib.inNixShell then null else ./.;
  propagatedBuildInputs = with pkgs;
   [ python3
     python3Packages.requests2
     python3Packages.redis
     python3Packages.future ];
}
